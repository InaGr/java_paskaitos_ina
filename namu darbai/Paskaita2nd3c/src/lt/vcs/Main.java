
package lt.vcs;

import static lt.vcs.VcsUtils.*;

public class Main {

    
    public static void main(String[] args) {
        String zod = inLine("Iveskite zodi/ sakini");
        String zodBe = zod.replaceAll(" ", "").replaceAll(",", "");
        String [] mas = zodBe.split("");
       
        int count = 0;
        
        for(int i = 0; i<mas.length; i++){
            if(zodBe.charAt(i) == 'a'){
                count++;
            }
        }
          
       out("Jus ivedete " + mas.length + " raidziu (-es).");
       out("Raide 'a' buvo ivesta: " + count);
    }
    
}
