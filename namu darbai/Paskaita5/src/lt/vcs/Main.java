
package lt.vcs;

import static lt.vcs.VcsUtils.*;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bankomatas bank = new Bankomatas();
        
        while (bank.getCash() > 0){
            String vardas = inStr("Iveskite savo varda");
            Person var1 = new Person(vardas);
            
            String pass = inStr("Iveskite PIN koda");
            bank.logIn(pass);
            
            int isimtaSuma = inInt("Kiek pinigu noretumete isimti?");
            bank.isimti(isimtaSuma);
            
            out("Jusu isimta suma: " + isimtaSuma + " Jusu saskaitos likutis: " + bank.getCash());
            
        }
        out("Pinigu likutis nepakankamas");
        System.exit(0);
    }
    
}
