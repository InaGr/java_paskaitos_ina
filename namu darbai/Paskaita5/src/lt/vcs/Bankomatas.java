
package lt.vcs;

import static lt.vcs.VcsUtils.*;


public class Bankomatas {    
    
    private int cash = 500;
    
    public boolean logIn (String pass){
        if (pass.equals("test")){
            out("PIN kodas teisingas");
            return true;            
        } else{
            out("PIN kodas neteisingas.");
            return logIn(inStr("Iveskite teisinga PIN koda")); //rekursinis metodas - metodas, kuris pats save iskviecia (savyje)
            
        }        
    }
    
    public void isimti (int sum){
        cash = cash - sum;        
    }
    
    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }
}
