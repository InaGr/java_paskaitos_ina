
package lt.vcs;

import java.util.Date;


public class Person {
    private String fName;
    private String lName;
    private Integer age;
    private Gender gender;
    private Date birthDate;
    
    public Person(String fName, String lName, Integer age, Gender gender){ //Date birthDate){
    this.age = age;
    this.fName = fName;
    this.lName = lName;
    this.gender = gender;
    //this.birthDate = birthDate;
} 
   
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public Integer getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }
    
    
    @Override // automatiskai prideda, cia tik zymejimui (nieko nereishkia)
    public String toString(){ // perrasem jau esame sistemoje metoda (pagal save - kaip musm reikia) - CIA VISISHKAI PAKEISTAS KODAS
        return "Person(vardas: " + fName + "; pavarde: " + lName + "; gimimo data: " + birthDate + ")";
    }
    
//    @Override
//    public String toString(){ // CIA tik PAPILDYTAS ESAMAS TEVINES KLASES KODAS
//        return super.toString() + "Person(vardas: " + fName + "; pavarde: " + lName + ")";
//    }

}
