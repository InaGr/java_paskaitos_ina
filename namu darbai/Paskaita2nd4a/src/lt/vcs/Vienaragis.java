
package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author tatu8
 */
public class Vienaragis extends Gyvunas {

    @Override
    public void gyvent() {
        out("Vienaragis gyvena");
        gyventKaipGyvunai();
    }

    @Override
    public String getWorld() {
        return "land";
    }
    
}
