
package lt.vcs;


public interface Creature { // nusakom taisykles, kurios visos impelementuotos klases - ju obejektai vykdys
    
    //visi metodai interfac'e yra ABSTRAKTUS ir  PUBLIC!!!!!
    //cia (t.y. interfac'e) metodai deklaruojami kitaip:    
//    public abstract String getWorld(); // cia ne {}, o tiesiog uzdarom su ';'
    
    //apacioj tarp * parasom dokumentacija
    /**
     * metodas kuris nusako kur karaliauja sis padaras, ar vandenyje, ar danguje, ar ant zemes
     * @return 
     */
    String getWorld(); //tiesiog parasom be zodziu "abstract" ir "public"
    
        
}
