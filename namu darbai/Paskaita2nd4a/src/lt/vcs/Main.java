
package lt.vcs;

import java.util.Date;
import java.util.Scanner;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Banknotas.*; //arba import lt.vcs.Banknotas
import lt.vcs.Gender;
import lt.vcs.Person;
import java.text.SimpleDateFormat;


public class Main {
    
    private static final String PIN = "test";// sukuriam lokalias konstantas
    
    private static String programName; 
    private static Date creationDate; 
    private static String creator; 
    
//    public Main(String name, String programer){ //naujas konstruktorius
//        this.programName = name;
//        this.creationDate = new Date();
//        this.creator = programer;
//    }
    
    public Main(String name, String programer){ // kitas budas ishkviesti naujame konstruktoriuje parametrus ish seno konstruktoriaus
        this(name); //kreipiasi i musu klases konstruktorius -  VISADA EINA PIRMAS TEIGINYS KONSTRUKTORIUJE!!!!!!!!!!!!!!
        this.creator = programer;
    }
    
    public Main(String name){ //naujas konstruktorius
        
        super(); //kreipiasi i tevynes klases konstruktorius!!!! - VISADA EINA PIRMAS TEIGINYS KONSTRUKTORIUJE!!!!!!!!!!!!!!
        
        if(name == null || name == ""){
            out("Programa turi tureti pavadinima.");
            System.exit(0);
        }
        this.programName = name; //'this' nusako imti ne paprasta kintamaji
        this.creationDate = new Date();
    }
    
    public static void main(String[] args) { // klase ishkart supaprasteja ir graziau atrodo, kad sukuri atskirus metodus
                    
//        paskaita3Kodas();            
//        paskaita3ndSprendimas();
        paskaita4Kodas();
    } 
    private static void paskaita4Kodas(){
        
        
        //cia skirta Object klasei (kuri sistemoj yra by default ir yra Tevu Tevas - pagrindine, t.y. pati pirmoji)
        Object o = new Object ();
        out("Object: " + o); //printina standartini vaizda
        o = new Arklys();
        out("Arklys: " + o); //printina jau musu sukurta vaizda, kuri sukurem klaseje Gyvunas (toSting)
        o = new Vienaragis();
        out("Vienaragis: " + o); //printina jau musu sukurta vaizda, kuri sukurem klaseje Gyvunas (toSting)
        
        
        //cia skirta interfac'ui Creature
        out("");
        Creature c = new Arklys();       
        Arklys ark = null;
        if(c instanceof Arklys){ //patikrinam kokios klases atstovas yra objektas, su f-ja 'instanceof'
            ark = (Arklys)c;
        }        
//        Arklys ark = (Arklys)c; //paverciam objekta i kita objekta
//        gyvent(ark);            //panaudojam paversta objekta
        gyvent (ark);
        out("Arklys: " + c);
        c = new Vienaragis();
        out("Vienaragis: " + c); 
        
        
        //cia skirta klasei Gyvunas
        out("");
        Gyvunas g = new Arklys();
        gyvent (g);
        out("Arklys: " + g);
        g = new Vienaragis();
        gyvent (g);
        out("Vienaragis: " + g);
        gyvent(new Arklys());
        
        
        //tirkinam ar objektas priklauso nurodytom klasem
        if(o instanceof Object){
            out("o yra Objektas");
        }        
        if(o instanceof Creature){
            out("o yra Creature");
        }        
        if(o instanceof Gyvunas){
            out("o yra Gyvunas");
        }        
        if(o instanceof Vienaragis){
            out("o yra Vienaragis");
        }
                        
    }
    
    private static void gyvent(Gyvunas g){
        g.gyvent();
    }
    
    // cia sukuriam nauja metoda kad supaprastinti kodo rasyma, kad trumpetu - LOGINIS ISSKAIDYMAS!!!
    private static void paskaita3ndSprendimas(){
        String name = inStr("Iveskite varda");
        String surname = inStr("Iveskite pavarde");
        int age = inInt("Iveskite amziu");
        int gen = inInt ("Pasirinkite lyti: 1-vyras; 2-moteris; 3-kita.");
       
        Person asmuo = new Person (name, surname, age, Gender.getById(gen));
        out(asmuo);
    }
    
    private static void paskaita3Kodas (){
        out(programName); 
        
        int suma = 350;
        int istraukti;
        String pin; 
        int i;
        
        out("Iveskite PIN koda");
        pin = new Scanner(System.in).next();

        for (i=1; i>0; i++){
        if (PIN.equals(pin)){
            out("PIN kodas teisingas. Jusu banko saskaitoje yra " + suma + " Eur. Prasome ivesti pageidaujama suma nuemimui:");
            out("Galite pasirinkti tik viena banknota, kiek noresite isgryninti");
            out("Banknoto nominalai: ");
            
            for (Banknotas bnkn : Banknotas.values()){ // graint visas banktonas, kurias sukurem
                out(bnkn.getLabel());
            }
            out("Kokia banknota norite pasirinkti?");
            istraukti = new Scanner(System.in).nextInt();
            while (istraukti>suma || istraukti<=0){
               out("Neteisinga suma.");
               out("Prasome ivesti pageidaujama suma nuemimui: ");
               istraukti = new Scanner(System.in).nextInt();
            }             
            Banknotas bnkn = suraskBanknota(istraukti);
            out("Jus istraukete " + istraukti + " Eur. Jusu banko saskaitos likutis " + (suma-istraukti) + " Eur. Jusu banknota: " + bnkn.getSk());
            break;               
                }else if (!PIN.equals(pin)){
                    out("PIN kodas ivestas neteisingai. Iveskite PIN koda");
                    pin = new Scanner(System.in).next();
                }
}
    }
    
    //pagalbinis metodas
    private static Banknotas suraskBanknota(int sk){
        Banknotas result = null;
        for (Banknotas bnkn : Banknotas.values()){ 
                if (bnkn.getSk() == sk){
                    result = bnkn;
                    break;
                }
            }
        return result;
    }
    
    
}
