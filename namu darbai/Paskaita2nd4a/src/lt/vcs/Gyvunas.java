
package lt.vcs;

import static lt.vcs.VcsUtils.*;


public abstract class Gyvunas implements Creature{ //kaip padarom klase "abstract" - ji NEPRIVALO impelemntuoti Creature metodus
    
    public abstract void gyvent(); // abstraktus 
    
    public void gyventKaipGyvunai(){ //paparastas metodas, ne abstraktus
        out("Gyvenu kaip gyvunas");
    }
    
    
    @Override
    public String toString(){
        return this.getClass().getName() + " " + getWorld(); // kadangi cia hierarhija, tai mes galime kviesti abstrakcius metodus neabstrakciame metode
    }
    
}
