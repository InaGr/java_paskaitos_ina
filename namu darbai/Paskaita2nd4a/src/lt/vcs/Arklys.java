
package lt.vcs;

import static lt.vcs.VcsUtils.*;

public class Arklys extends Gyvunas { // extends - prapliecia; kaip praplieciam klase, automatishkai paveldem visa hierarhija tos klases
    
    public void gyvent(){
        out("Arklys gyvena");
        gyventKaipGyvunai();
    }

    @Override
    public String getWorld() {
        return "land";
    }
       
    
}
