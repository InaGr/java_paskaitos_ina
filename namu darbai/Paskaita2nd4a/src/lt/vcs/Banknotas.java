
package lt.vcs;


public enum Banknotas { 
    
PENKI(5, "penki"),
DESIMT(10, "desimt"),
DVIDESIMT(20, "dvidesimt"),
PENKIASDESIMT(50, "penkiasdesimt"),
SIMTAS(100, "simtas");

private int sk;
private String label;
 

   
private Banknotas(int sk, String label){ //cia konstruktorius. enum'e konstruktoriai rashosi kitaip, jis visada PRIVATE
    this.sk = sk;
    this.label = label;
}

 public int getSk() { // kad PRIVATE pasiekti is isores
        return sk;
    }
   
    public String getLabel() {
        return label;
    }
    
}
// istrinat SETTER'ius, kad is isores niekas nepasiektu
