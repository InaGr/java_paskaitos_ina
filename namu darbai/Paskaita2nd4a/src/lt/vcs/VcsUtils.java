
package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;


public class VcsUtils {
    
    //private Main pvz = new Main(" ");
    
    public static void out(Object txt) {
        System.out.println(txt); //iskviecia bet kokio objekto .toString metoda
    }
    
    public static void outTime(Object txt) {
        System.out.println(timeNow() + " " + txt); //iskviecia bet kokio objekto .toString metoda
    }
    
    public static String inStr (String txt){ // priima tik viena zodi!!!
        out(txt);
        return inStr();
    }
    
    public static String inStr(){
        return newScan().next();//'nextLine' - atspausdintu visa eilute, o 'next' spausdina tik 1-a zodi
    }
    
    public static int inInt(String txt){
        out(txt);
        return inInt();
    }
           
    public static int inInt(){
        return newScan().nextInt();
    }
    
    public static String inLine(String txt){ // priima visa eilute!!!!
        out(txt);
        return inLine();        
    }
    
    public static String inLine(){
        return newScan().nextLine();
    }
    
    private static Scanner newScan(){
        return new Scanner(System.in);
    }
    private static String timeNow(){
        SimpleDateFormat time = new SimpleDateFormat ("'['HH:mm:ss:SS']'");
        return time.format(new Date());
    }
    
    public static int random(int from, int to){
        return ThreadLocalRandom.current().nextInt(from, to + 1);
    }
}
