package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        out("Kauliuku pokeris");
        String p1name = inStr("Zaidejas 1, iveskite savo varda");
        String p2name = inStr("Zaidejas 2, iveskite savo varda");
        
        Player p1 = new Player(p1name);
        Player p2 = new Player(p2name);
        boolean zaisti = true;      
                
        while (zaisti) {
            Game newGame = new Game(p1, p2);
            Player nugaletojas = newGame.start(); 
            Player pralaimetojas = getPralaimetojas(newGame, nugaletojas);

            out(nugaletojas.getName() + " Jusu likutis: " + nugaletojas.getCash());
            out(pralaimetojas.getName() + " Jusu likutis: " + pralaimetojas.getCash());
            
            int choice = inInt("Ka norite daryti toliau: 0-zaisti dar; 1-baigti zaidima.");
            
            if(choice == 1){
                break;
            }
            
//            out("Zaidejas " + p1name + " - kiek noretumete statyti?");
//            int p1StatSuma = inInt();
//                                    
//            out("Zaidejas " + p2name + " - ar noretumete pastatyti tiek pat ar daugiau?");
//            int p2StatSuma = inInt();
//                        
//            int i = 0;
//            while (p2StatSuma != p1StatSuma){
//                if (p2StatSuma > p1StatSuma+i){
//                    out("Zaidejas " + p1name + " - ar noretumete pastatyti tiek pat ar daugiau?"); 
//                    p1StatSuma = inInt();
//                }else {
//                    out("Zaidejas " + p2name + " - ar noretumete pastatyti tiek pat ar daugiau?"); 
//                    p2StatSuma = inInt();
//                } 
//                i++;
//                }
//            p1.setCash(p1.getCash()-p1StatSuma);
//            int p1Cash = p1.getCash();
//            p2.setCash(p2.getCash()-p2StatSuma);
//            int p2Cash = p2.getCash();
//            
//            out("Zaidejo " + p1name + " statymo suma: " + p1StatSuma + ". Pinigu likutis: " + p1Cash); 
//            out("Zaidejo " + p2name + " statymo suma: " + p2StatSuma + ". Pinigu likutis: " + p2Cash);
//            
//            out("Zaidejas " + p1name + " meskite kauliukus");
//                                                                       
//            GameUtils.rollHand();
//            out(GameUtils.intArrayToString(rollHand()));
             
        }
        
    }
    
    private static Player getPralaimetojas (Game game, Player winner){
        if (winner.equals(game.getP1())){
           return game.getP2();
        }else{
            return game.getP1();
        }        
    } 
    
    public static Player getNextActivePlayer (Game game){
        if (game.getActivePlayer().equals(game.getP1())){
            return game.getP2();
        }else{
            return game.getP1();
        }
    }    
        
}
