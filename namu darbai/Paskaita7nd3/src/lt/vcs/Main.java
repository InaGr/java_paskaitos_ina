
package lt.vcs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static lt.vcs.VcsUtils.*;

public class Main {

    
    public static void main(String[] args) {
       String eilute = inLine("Iveskite teksta");
       String isvalytaEilute = eilute.replaceAll(" ", "").replaceAll(",", "");
       char[] raides = isvalytaEilute.toCharArray();
       List<String> strRaides = charArrToStrList(raides);
       Map<String, Integer> mapas = new HashMap();
       for(String raide : strRaides){
           Integer value = mapas.get(raide);
           if(value == null){
               mapas.put(raide, 1);
           } else {
               mapas.put(raide, value+1);
           }
       }
       
       List<Integer> values = new ArrayList(mapas.values());
       Collections.sort(values);
       Collections.reverse(values);
       out("Panaudotos raides mazejimo tvarka, ignoruojant didziasias ar mazasias raides");
       for(Integer sk : values){
           List<String> panaudotos = new ArrayList();
                      for(String key : mapas.keySet()){
               Integer val = mapas.get(key);
               if(!panaudotos.contains(key) && sk.equals(val)){
                   panaudotos.add(key);
                   out(key + " - " + sk);
                   break;
               }
           }
       }
       
    }
    private static List<String> charArrToStrList (char[] chars){
        List<String> result = new ArrayList();
        for (char charas : chars){
            result.add("" + charas);
        }
        return result;
    }
}
