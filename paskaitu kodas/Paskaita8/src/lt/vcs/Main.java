
package lt.vcs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

public class Main {

   
    public static void main(String[] args) throws Exception {
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "vcs";
        String dbUser = "root";
        String dbPass = "";
        Connection conn = DriverManager.getConnection(url + dbName, dbUser, dbPass);
        if(conn != null){
            out("Prisijungimas prie " + url+dbName  + " sekmingas!");
        }
        
        String name = inStr("Iveskite varda");
        String surname = inStr("Iveskite pavarde");
        int age = inInt("Iveskite amziu");
        Person p = new Person(getLastPersonId(conn)+1, name, surname, age); 
        if (p.save(conn)) {
            out("Person (id = " + p.getId() +  ") objektas issaugotas!");
        } else {
            out("Person objekto issaugoti nepavyko!");
        }
        conn.commit(); //pries uzdarant connection issaugo visas operacijas (tik tuo atveju jeigu visos operacijos praejo sekmingai!
        conn.close(); //po issaugojimo - uzdarom connection

    }
    
    private static int getLastPersonId(Connection conn) throws Exception{
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select id from person order by id desc limit 1");
        rs.next();
        return rs.getInt("id");
    }
    
        
}
