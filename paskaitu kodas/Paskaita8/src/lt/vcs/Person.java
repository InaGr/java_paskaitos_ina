
package lt.vcs;

import java.sql.Connection;
import java.sql.Statement;


public class Person {
    
    private int id;
    private String name;
    private String surname;
    private int age;
    
    public Person(int id, String name, String surname, int age){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
    
    public boolean save (Connection conn) throws Exception{
        Statement st = conn.createStatement();
        int res = st.executeUpdate("insert into person values(" + id + ",'" + name + "','" + surname + "'," + age + ");");
        return res>0;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
}
