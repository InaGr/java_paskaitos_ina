package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import static lt.vcs.VcsUtils.*; //* - reiskia bet koks simbolis - reiskia mes importuojam visus narius is sukurtos klases

public class Main {
    
    public static void main(String[] args) {
        
        String ivestas;
      while (true){
          System.out.println("Iveskite zodi");
          ivestas = new Scanner(System.in).next();            
          if(ivestas.equals("pabaiga")||ivestas.equals("end")||ivestas.equals("exit")){
          break;
      }
      }
      int result = 0;
      while (true){
         int i = inInt ("Ivesk sveika skaciu");
         if (i ==0){
             break;
         }else{
             result +=i;
         }
      }
      out ("Rezultatas: " + result);
      
      switch (ivestas){ //realiai cia tas pats kaip ir IF
          case "pabaiga":
              //rasom koda jei tekstas pabaiga
              break;
          case "end":
              //rasom koda jei tekstas end
              break;
          case "exit":
              //rasom koda jei tekstas exit
              break;
          default:
              //jei neatitiko ne vienai salygai pries tai
      }
      
      int i = inInt("Ivesk sveika skaciu");
      
//      for(; i>0; i--){ //FOR salygu visu galima nerasyt, privaloma yra VIDURINE salyga, bet reikia rasyt kabliataskius
//          if (i == 3){
//              continue; ///persoka viena vykdyma, siuo atveju persoka skaiciu 3 (jo neraso)
//          }
//          if (i == 1){
//              break;
//          }
//          out(""+i); //cia kaip apeiti jeigu musu kintamasis NE String, o skaicius
//      }
//      out("Rezultatas i lygus: "+i);

      
      int[] mas = i>0 ? new int[i] : new int [5]; //alternatyva IF salygai - po "?" true salyga, o po ":" - false salyga
      int[] mas2 = {1,2,3,4,5};         // 2-as budas sukurti masyva
      String[] mas3 = {"ddd","fsff"};   // 3-as budas sukurti masyva
      int [] mas4;                      // 4-as budas sukurti masyva
      
      Object o = mas3;
      
      //Arrays. // leidzia ish saraso pasirinkti daugiau funkciju (search, sort, etc.)
      //List<String>mas3 = Arrays.asList(mas3)// pavercia masyva i sarasa
      
      String formatuojam = "mano batai buvo %d";
      out (String.format(formatuojam, 7));
      out (formatuojam);
      
      String kint = null; // siuo atveju jeigu kintamasis yra NULL, tada reikai IF salygos, kad programa neuzluztu
      if (kint != null){
          kint.isEmpty();
      }
      
            
      for(int a = mas.length-1; a>0; a--){ //FOR salygu visu galima nerasyt, privaloma yra VIDURINE salyga, bet reikia rasyt kabliataskius
        mas[a]=a;  
      }
      
      for (int a : mas){ //FOREACH ciklas - ims kiekviena nari ish masyvo MAS 
          out (""+a);
      }
      
      while(i>0){
          ///kodas
      }
      
      do { // skiriasi nuo WHILE, kad pirma vykdys koda, o po to ivikdys salyga while
          //kodas
      } while (i>0);      
      
      
      out("Rezultatas i lygus: "+i);
      
      
      
      //KAIP SUKURTI DATA!!!!!
      SimpleDateFormat ymd = new SimpleDateFormat("yyyy-MM-dd '['HH:mm:ss']'"); //idedam simboli i viengubas kabutes '['
      Date data = new Date();       
      out(data);
      out(ymd.format(data)); //parodo tokiame formate kaip mes praseme (uzklauseme)
      
      for (int ii =0; ii<5; ii++){
          out("random" + ii + ": " + random(1,6));      
    }   
    }   

}
