package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;



/**
 *
 * @author Cukrus
 */
public class VcsUtils {
    
    private static final String NL = System.lineSeparator(); 
    
    public static void out(Object txt) {
        System.out.println(timeNow() + " " + txt);
    }
    
    public static String inStr(String txt) {
        out(txt);
        return inStr();
    }
    
    public static String inStr() {
        return newScan().next();
    }
    
    public static int inInt(String txt) {
        out(txt);
        return inInt();
    }
    
    public static String inLine(String txt) {
        out(txt);
        return inLine();
    }
    
    public static String inLine() {
        return newScan().nextLine();
    }
    
    public static int inInt() {
        return newScan().nextInt();
    }
    
    private static Scanner newScan() {
        return new Scanner(System.in);
    }
    
    private static String timeNow() {
        SimpleDateFormat sdf = new SimpleDateFormat("'['HH:mm:ss.SSS']'");
        return sdf.format(new Date());
    }
    
    public static int random(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to + 1);
    }
    
    public static String readTextFile (BufferedReader br) throws Exception {
        String tekstas = "";  
        String nelygu;
        while (true){
            nelygu = br.readLine();
            if(nelygu == null){
                break;
            }
            tekstas += nelygu + NL;            
        }
        //Arba galima daryti ir taip - 2 budas
//        while ((nelygu = br.readLine()) != null){
//            tekstas += nelygu + NL; 
//        }       
        return tekstas;
    }
    
    public static BufferedWriter createBW (File file) throws Exception {
        FileOutputStream fos = new FileOutputStream (file);
        OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
        BufferedWriter bw = new BufferedWriter(osw);
        return bw;
    }
    
    public static BufferedReader createBR (File file) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
        BufferedReader br = new BufferedReader(isr);
        return br;
    }
    
}
