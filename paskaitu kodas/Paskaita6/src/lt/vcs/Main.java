
package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import static lt.vcs.VcsUtils.*;


public class Main {
    
    private static final String NL = System.lineSeparator(); // paraso ish naujos eilutes

    
    public static void main(String[] args) throws Exception { // "throws Exception" - ivesti tam, kad FileInputStream suveiktu, reiskia "kad gali ismesti klaida"
        out(" bla \t bla"); // nes vienas back-slash naudojamas ishvesti kazkokius simbolius, kaip cia - "\t"
        String ivestasFile = inStr("Iveskite failo pavadinima (su visu keliu)");
        File failas = new File(ivestasFile);
//        File failas = new File("C:\\pvz.txt"); // jeigu rasom su back-slash, tada reiketu naudoti dviguba back-slash
        out("Failas egzistuoja? " + failas.exists());
                
        String failoTurinys = "";        
        
        //norint prie jau esamo failo prideti naujas eilutes
//        try{ //cia dabar gaudom klaidas su try-catch bloku
//        FileInputStream fis2 = new FileInputStream(failas);
//        InputStreamReader isr2 = new InputStreamReader(fis2, "UTF-8");
//        BufferedReader br2 = new BufferedReader(isr2);
//        failoTurinys = readTextFile(br2);
//        br2.close();
//        }catch (Exception ex){
//            //out("Ivyko klaida. Failas nerastas." + fnfe.getMessage());
//            //throw fnfe; // su "throw" nutraukiamas programos darbas, jeigu "throw" nera - programa veikia toliau
//            throw new RuntimeException("Ivyko klaida", ex);
//        }
        
        //arba pridedi tiek "catch" kiek reikia - visi Exception'ai turi buti hierarhijos tvarka, t.y. pradedam nuo TEVO, po to vaikai 
//        }catch (FileNotFoundException fnfe){            
//            throw new RuntimeException("", fnfe);
//        } catch (UnsupportedEncodingException ex2){            
//            throw new RuntimeException("", fnfe);
                            
        //kaip IRASYTI i faila kazka
//        FileOutputStream fos = new FileOutputStream (failas);
//        OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
//        BufferedWriter bw = new BufferedWriter(osw);
        
               
        
        //pridedam nauja eilute i esama faila
        BufferedWriter bw = createBW(failas); //reikia priskirti kintamaji, norint iskviesti metoda
        bw.append(failoTurinys); //norint prie jau esamo failo prideti naujas eilutes
        bw.newLine();
        bw.append("pirmas irasymas");
        bw.flush();
        bw.close();
        
        //kaip NUSKAITYTI is failo
//        FileInputStream fis = new FileInputStream(failas);
//        InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
//        BufferedReader br = new BufferedReader(isr); //BufferedReader apdoroja eilute po eilutes
//        out("Ivesto failo 1 eilute: " + br.readLine());
//        out("Ivesto failo 2 eilute: " + br.readLine());
        BufferedReader br = createBR(failas);
        
        out("Teksto turinys: " + NL + readTextFile(br));       
                
        br.close(); // reikia butinai uzdaryti atidarytus Stream'us, kad nenaudotu veltui kompo resursus
    }
    
    
    
}
