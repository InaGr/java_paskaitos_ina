
package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Deimantas.*;

public class Main {

 
    public static void main(String[] args) {
       String words = inLine("Iveskite zodzius atskirtus kableliu");
       String [] wordMas = words.replaceAll(" ", "").split(",");
       
       //keliu lygiu masyvo pvz
       int[][] intMas = { 
           {1,2,3}, //eilute atitinka 0
           {4,5,6}, //eilute atitinka 1
           {7,8,9}  //eilute atitinka 2
       };
    out("" + intMas[1][1]); //antrose skliaustuose nurodom pasirinktoj eilutej skaciaus pozicija (nepamirshti kad prasideda nuo 0)
    
       List <String> pvz = new ArrayList();
       pvz.add("kazkas");
    
       List <String> listas = toSortedList(wordMas); //irasome <String> - reiskia nurodome kad musus masyvas susideda ish String subjektu
       
       Integer[] mas = {3,2,1};
       List<Integer> inLst = toSortedList(mas);
       
       out("Surusiuoti zodziai liste:");
       for (String word : listas){
           out(word);
       }
       Collections.sort(listas);
       Set <String> setas = new TreeSet(listas);
       out("Surusiuoti zodziai:");
       for (String word : setas){
           out(word);
       }
       
       Map<Integer, String> mapas = new HashMap(); //Map'ai deklaruojami su 2 subjektais: 1 reiksme - raktas KEY (kuris nusako subjektas, t.y. 2 reiksme)
                                                   //(visi KEY yra unikalus, jie negali dubliuotis), raktai laikomi SET'e,
                                                   //o 2 reiksme - subjektas VALUE
       mapas.put(1, "bananas");
       mapas.put(2, "ananasas");
       out(mapas.get(2));
       
       Map<String, List<String>> mapas2 = new HashMap();
       List<String> listas2 = Arrays.asList("ananasas", "bananas", "agurkas");
       mapas2.put("nesurusiuotas", listas2);
       mapas2.put("surusiuotas", listas);
       out(mapas2.get(2));
       for (String word : mapas2.get("nesurusiuotas")){
           out(word);
       }
       
       Deimantas<String> bla = null; //leidzia nurdyti String tipa, nes klasej Deimantas nurodytas apribojimas <D>
       
       
    }
    
//    private static List <String> toSortedList (String[] strMas){ //irasom kad musu metodas grazina butent String list'a - reiskia papildom su <String>
//        List <String> listas = Arrays.asList(strMas);
//        Collections.sort(listas);
//        return listas;
//        
//    }
    
    private static <E> List <E> toSortedList (E... strMas){ //irasom kad musu metodas grazina butent String list'a - reiskia papildom su <String>
        List <String> listas = Arrays.asList(strMas);
        Collections.sort(listas);
        return listas;
        
    }
    
    
}
